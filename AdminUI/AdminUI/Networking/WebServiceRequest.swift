//
//  WebServiceRequest.swift
//  AdminUI
//
//  Created by Ann Mary on 23/10/21.
//

import Foundation
import RxSwift

class WebServiceRequest {
    
    static let shared = WebServiceRequest()
    let header = ["Content-Type": "application/json"] as [String: String]
    
    func fetchMembers(handler: @escaping WebServiceCompletionHandler) {
        
        let url = base_url + endpoint
        
        WebServiceManager.sharedInstance.requestArray(with: url, method: .get, parameter: nil, header: header, mappableOf: MemberResponseModel.self, completionHandler: handler)
    }
    
    
    
}
