//
//  WebServiceManager.swift
//  AdminUI
//
//  Created by Ann Mary on 23/10/21.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftyJSON

class WebServiceManager {
    
    static let sharedInstance = WebServiceManager()
    fileprivate var completionHandler: WebServiceCompletionHandler?
    
    // MARK: - Webservice
    func request<T: Mappable>(with url: String, method: HTTPMethod, parameter: [String: Any]?, header: HTTPHeaders?, mappableOf: T.Type, completionHandler: @escaping WebServiceCompletionHandler) {
        
        print("--- URL --- : \(url)")
        
        DispatchQueue.global(qos: .background).async {
            // Alamofire Request
            Alamofire.request(url, method: method, parameters: parameter, encoding: JSONEncoding.default, headers: header)
                .validate()
                .responseObject { (response: DataResponse<T>) in
                    
                    switch response.result {
                    
                    case .success(let successResult): // Success 200
                        dump(successResult)
                        completionHandler(true, nil, successResult as AnyObject)
                        
                    case.failure(_): // Failed
                        completionHandler(false, "Oops failed ...", nil)
                    }
                }
        }
    }
    
    func requestArray<T: Mappable>(with url: String, method: HTTPMethod, parameter: [String: Any]?, header: HTTPHeaders?, mappableOf: T.Type, completionHandler: @escaping WebServiceCompletionHandler) {
            
            print("--- URL --- : \(url)")
            
            
            // Start the long-running task and return immediately.
            DispatchQueue.global(qos: .background).async {
                
                Alamofire.request(url, method: method, parameters: parameter, encoding: JSONEncoding.default, headers: header)
                    .validate()
                    .responseArray { (response: DataResponse<[T]>) in
                        switch response.result {
                        
                        case .success(let successResult): // Success 200
                            dump(successResult)
                            completionHandler(true, nil, successResult as AnyObject)
                        case.failure(_): // Failed
                            completionHandler(false, "Oops failed ..." , nil)
                        }
                    }
            }
        }
    
}
