//
//  MemberRealmModel.swift
//  AdminUI
//
//  Created by Ann Mary on 24/10/21.
//

import Foundation
import RealmSwift

class MemberRealmModel: Object {
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    @objc dynamic var email = ""
    @objc dynamic var role = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
