//
//  MemberResponseModel.swift
//  AdminUI
//
//  Created by Ann Mary on 23/10/21.
//

import Foundation
import ObjectMapper

class MemberResponseModel: Mappable {
    
    var id: String?
    var name: String?
    var email: String?
    var role: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        email <- map["email"]
        role <- map["role"]
    }
}
