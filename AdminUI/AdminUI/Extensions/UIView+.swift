//
//  UIView+.swift
//  AdminUI
//
//  Created by Ann Mary on 23/10/21.
//

import Foundation
import UIKit

extension UIView {
    
    func dropShadow(){
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 9.0
        self.layer.masksToBounds = false
    }
}
