//
//  EditViewController.swift
//  AdminUI
//
//  Created by Ann Mary on 23/10/21.
//

import UIKit
import RealmSwift

class EditViewController: UIViewController {

    //MARK:- IB Outlets
    @IBOutlet weak var txtEditName: UITextField!
    @IBOutlet weak var txtEditEmail: UITextField!
    @IBOutlet weak var txtRole: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    
    //MARK:- Variable Declarations
    var pickerview = UIPickerView()
    var id = ""
    lazy var members: Results<MemberRealmModel> = { realm.objects(MemberRealmModel.self) }()
    
    var arrRoles = ["-- Select --", "Admin", "Member"]

    //MARK:- Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        customizeUI()
    }

    //MARK:- IB Actions
    @IBAction func onBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSaveClick(_ sender: UIButton) {
        let users = members.filter("id = %@", id)
        
        if let member = users.first {
            try! realm.write {
                member.name = txtEditName.text!
                member.email = txtEditEmail.text!
                member.role = txtRole.text!
            }
        }
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK:- Custom Methods
    func customizeUI() {
        btnSave.layer.cornerRadius = 8.0
        let user = members.filter("id = %@", id).first!
        
        txtEditName.text = user.name
        txtEditEmail.text = user.email
        txtRole.text = user.role.capitalizingFirstLetter()
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.init(named: "StrongBlue")
            toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onDoneClick))
        
        let flexibleButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        
        toolBar.setItems([flexibleButton , doneButton], animated: true)
        
        txtRole.inputView = pickerview
        txtRole.inputAccessoryView = toolBar
        pickerview.delegate = self
        pickerview.dataSource = self
        
    }
    
    //MARK:- Custom Actions
    
    @objc func onDoneClick() {
        txtRole.resignFirstResponder()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK:- UIPickerViewDelegate
extension EditViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrRoles[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtRole.text = "\(arrRoles[row])"
    }
}

//MARK:- UIPickerViewDataSource
extension EditViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrRoles.count
    }
    
    
}
