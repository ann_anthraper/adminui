//
//  UserListViewController.swift
//  AdminUI
//
//  Created by Ann Mary on 23/10/21.
//

import UIKit
import RealmSwift

class UserListViewController: UIViewController {
    
    //MARK:- IB Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicatorBGView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnDeleteSelected: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK:- Variable Declarations
    var currentPage: Int = 1
    var editProfileID: Int?
    var isFiltered = false

    var arrSelectedRows:[Int] = []
    
    var arrMembers = [MemberResponseModel]()
    var arrPaginationContent = [MemberRealmModel]()
    var arrFilterContents = [MemberRealmModel]()
    lazy var members: Results<MemberRealmModel> = { realm.objects(MemberRealmModel.self) }()
    
    let columnLayout = FlowLayout(
                itemSize: CGSize(width: 30, height: 30),
                minimumInteritemSpacing: 10,
                minimumLineSpacing: 0,
            sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            )

    
    //MARK:- Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        customizeUI()
        registerCustomCell()
        fetchMembersList()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        isFiltered = false
        searchBar.text = nil
        heightConstraint.constant = 100
        self.view.endEditing(true)
    }
    
    //MARK:- Custom Methods
    func customizeUI() {
        bottomView.dropShadow()
        btnDeleteSelected.layer.cornerRadius = 6.0
        activityIndicatorBGView.layer.cornerRadius = 10.0
        tableView.isHidden = true
        btnDeleteSelected.isHidden = true
        collectionView.collectionViewLayout = columnLayout
        tableView.tableFooterView = UIView()
    }
    
    func registerCustomCell() {
        let tableViewNib = UINib(nibName: "UserTableViewCell", bundle: nil)
        tableView.register(tableViewNib, forCellReuseIdentifier: "UserTableViewCell")
        let tableViewLoadingNib = UINib(nibName: "LoadMoreTableViewCell", bundle: nil)
        tableView.register(tableViewLoadingNib, forCellReuseIdentifier: "LoadMoreTableViewCell")
        let collectionViewNib = UINib(nibName: "PaginationCollectionViewCell", bundle: nil)
        collectionView.register(collectionViewNib, forCellWithReuseIdentifier: "PaginationCollectionViewCell")
    }
    
    func fetchMembersList() {
        try! realm.write {
            realm.deleteAll()
        }
        WebServiceRequest.shared.fetchMembers { (status, message, responseObject) in
            self.arrMembers.removeAll()
            self.arrPaginationContent.removeAll()
            switch status {
            case true:
                if let response = responseObject as? [MemberResponseModel] {
                    self.arrMembers = response
                    maximumPageCount = (self.arrMembers.count.roundedUp(toMultipleOf: 10))/10
                    self.activityIndicator.stopAnimating()
                    self.updateUI()
                }
            case false:
                if let errMessage = message {
                    print(errMessage)
                }
            }
        }
    }
    
    func updateUI() {
        for index in 0 ..< arrMembers.count {
            let member = MemberRealmModel()
            member.id = arrMembers[index].id ?? ""
            member.name = arrMembers[index].name ?? ""
            member.email = arrMembers[index].email ?? ""
            member.role = arrMembers[index].role ?? ""
            
            try! realm.write {
                realm.add(member)
            }
        }
        updatePaginationContent(page: currentPage)
        activityIndicator.stopAnimating()
        activityIndicatorBGView.isHidden = true
        tableView.isHidden = false
    }
    
    func updatePaginationContent(page:Int) {
        if page > 0 {
            maximumPageCount = (members.count.roundedUp(toMultipleOf: 10))/10
            arrPaginationContent.removeAll()
            let limit = (page == maximumPageCount) ? members.count : (paginationDataLimit * page)
            for index in 0 ..< limit {
                arrPaginationContent.append(members[index])
            }
            collectionView.reloadData()
            tableView.reloadData()
            tableView.scrollToRow(at: IndexPath(row: (paginationDataLimit * (page - 1)), section: 0), at: .top, animated: false)
        }
    }
    
    //MARK:- IB Actions
    @IBAction func onBtnDeleteSelectedClick(_ sender: UIButton) {
        for index in arrSelectedRows {
            if let member = members.filter("id = %@", String(index)).first {
                try! realm.write {
                    realm.delete(member)
                }
            }
        }
        arrSelectedRows.removeAll()
        isFiltered = false
        heightConstraint.constant = 100
        searchBar.text = nil
        self.view.endEditing(true)
        btnDeleteSelected.isHidden = true
        updatePaginationContent(page: currentPage)
    }
    
    //MARK:- Custom Actions
    @objc func onSelectMember(_ sender: UIButton) {
        if arrSelectedRows.contains(sender.tag) {
            arrSelectedRows.remove(at: arrSelectedRows.firstIndex(of: sender.tag)!)
        } else {
            arrSelectedRows.append(sender.tag)
        }
        
        btnDeleteSelected.isHidden = (arrSelectedRows.count == 0) ? true : false
        tableView.reloadData()
        
    }
    
    @objc func onEditProfile(_ sender: UIButton) {
        editProfileID = sender.tag
        self.performSegue(withIdentifier: "edit-profile", sender: self)
    }
    
    @objc func onDelete(_ sender: UIButton) {
        if let member = members.filter("id = %@", String(sender.tag)).first {
            if isFiltered {
                if let index = arrFilterContents.firstIndex(of: member) {
                    arrFilterContents.remove(at: index)
                    try! realm.write {
                        realm.delete(member)
                    }
                    tableView.reloadData()
                }
            } else {
                try! realm.write {
                    realm.delete(member)
                }
                updatePaginationContent(page: 1)
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "edit-profile" {
            let vc = segue.destination as? EditViewController
            if let id = editProfileID {
                vc?.id = String(id)
            }
        }
    }
}

//MARK:- UITableViewDelegate
extension UserListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 100
        case 1:
            return (currentPage == maximumPageCount) ? 0 : 50
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 1:
            if currentPage < maximumPageCount {
                currentPage += 1
                updatePaginationContent(page: currentPage)
            }
        default:
            break
        }
    }
}

//MARK:- UITableViewDataSource
extension UserListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return isFiltered ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return isFiltered ? arrFilterContents.count : arrPaginationContent.count
        case 1:
            return 1
        default:
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as? UserTableViewCell {
                
                cell.selectionStyle = .none
                
                if isFiltered {
                    cell.lblRole.text = arrFilterContents[indexPath.row].role.capitalizingFirstLetter()
                    switch arrFilterContents[indexPath.row].role {
                    case "admin":
                        cell.roleBGView.backgroundColor = UIColor.init(named: "StrongBlue")
                    case "member":
                        cell.roleBGView.backgroundColor = UIColor.init(named: "StrongGreen")
                    default:
                        cell.roleBGView.backgroundColor = .clear
                    }
                    
                    cell.lblName.text = arrFilterContents[indexPath.row].name
                    cell.lblEmail.text = arrFilterContents[indexPath.row].email
                    
                    cell.btnSelectCell.tag = Int(arrFilterContents[indexPath.row].id)!
                    cell.btnEditProfile.tag = Int(arrFilterContents[indexPath.row].id)!
                    cell.btnDeleteUser.tag = Int(arrFilterContents[indexPath.row].id)!
                    
                    if arrSelectedRows.contains(Int(arrFilterContents[indexPath.row].id)!) {
                        cell.containerView.backgroundColor = UIColor.init(named: "SilverGrey")
                        cell.btnSelectCell.setImage(UIImage(named: "ic-checked"), for: .normal)
                    } else {
                        cell.containerView.backgroundColor = UIColor.white
                        cell.btnSelectCell.setImage(UIImage(named: "ic-unchecked"), for: .normal)
                    }
                    
                } else {
                    cell.lblRole.text = arrPaginationContent[indexPath.row].role.capitalizingFirstLetter()
                    switch arrPaginationContent[indexPath.row].role {
                    case "admin":
                        cell.roleBGView.backgroundColor = UIColor.init(named: "StrongBlue")
                    case "member":
                        cell.roleBGView.backgroundColor = UIColor.init(named: "StrongGreen")
                    default:
                        cell.roleBGView.backgroundColor = .clear
                    }
                    
                    cell.lblName.text = arrPaginationContent[indexPath.row].name
                    cell.lblEmail.text = arrPaginationContent[indexPath.row].email
                    
                    cell.btnSelectCell.tag = Int(arrPaginationContent[indexPath.row].id)!
                    cell.btnEditProfile.tag = Int(arrPaginationContent[indexPath.row].id)!
                    cell.btnDeleteUser.tag = Int(arrPaginationContent[indexPath.row].id)!
                    
                    if arrSelectedRows.contains(Int(arrPaginationContent[indexPath.row].id)!) {
                        cell.containerView.backgroundColor = UIColor.init(named: "SilverGrey")
                        cell.btnSelectCell.setImage(UIImage(named: "ic-checked"), for: .normal)
                    } else {
                        cell.containerView.backgroundColor = UIColor.white
                        cell.btnSelectCell.setImage(UIImage(named: "ic-unchecked"), for: .normal)
                    }
                }
                
                cell.btnEditProfile.addTarget(self, action: #selector(onEditProfile(_:)), for: .touchUpInside)
                cell.btnDeleteUser.addTarget(self, action: #selector(onDelete(_:)), for: .touchUpInside)
                cell.btnSelectCell.addTarget(self, action: #selector(onSelectMember(_:)), for: .touchUpInside)
                
                return cell
            }
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreTableViewCell", for: indexPath) as? LoadMoreTableViewCell {
                cell.selectionStyle = .none
                return cell
            }
        default:
            return UITableViewCell()
        }
        return UITableViewCell()
    }
}

//MARK:- UICollectionViewDelegate
extension UserListViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let rows = collectionView.numberOfItems(inSection: indexPath.section)
        switch indexPath.row {
        case 0:
            currentPage = 1
            updatePaginationContent(page: currentPage)
        case 1:
            if currentPage > 1 {
                currentPage -= 1
                updatePaginationContent(page: currentPage)
            }
        case rows - 1:
            currentPage = maximumPageCount
            updatePaginationContent(page: currentPage)
        case rows - 2 :
            if currentPage < maximumPageCount {
                currentPage += 1
                updatePaginationContent(page: currentPage)
            }
        default:
            currentPage = indexPath.row - 1
            updatePaginationContent(page: currentPage)
            break
        }
    }
}

//MARK:- UICollectionViewDataSource
extension UserListViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return maximumPageCount + 4 // 4: left and right buttons for previous & next page
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PaginationCollectionViewCell", for: indexPath) as? PaginationCollectionViewCell {
            
            let rows = collectionView.numberOfItems(inSection: indexPath.section)
            cell.imgView.isHidden = false
            switch indexPath.row {
            case 0:
                cell.imgView.image = UIImage(named: "ic-first-page")
                cell.lblTitle.isHidden = true
            case 1:
                cell.imgView.image = UIImage(named: "ic-previous")
                cell.lblTitle.isHidden = true
            case rows - 1:
                cell.imgView.image = UIImage(named: "ic-last-page")
                cell.lblTitle.isHidden = true
            case rows - 2:
                cell.imgView.image = UIImage(named: "ic-next")
                cell.lblTitle.isHidden = true
            default:
                cell.imgView.isHidden = true
                cell.lblTitle.isHidden = false
                cell.lblTitle.layer.cornerRadius = cell.frame.height/2
                let value = (indexPath.row + 1) - 2
                if value == currentPage {
                    cell.lblTitle.backgroundColor = UIColor.init(named: "StrongBlue")
                    cell.lblTitle.textColor = .white
                } else {
                    cell.lblTitle.backgroundColor = .white
                    cell.lblTitle.textColor = UIColor.init(named: "StrongBlue")
                }
                cell.lblTitle.text = String(value)
            }
            return cell
        }
        return UICollectionViewCell()
    }
}

//MARK:- UICollectionViewDelegateFlowLayout
extension UserListViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let rows = collectionView.numberOfItems(inSection: indexPath.section)
        
        switch indexPath.row {
        case 0, 1, rows - 1, rows - 2 :
            return CGSize(width: 40, height: 40)
        default:
            return CGSize(width: 30, height: 30)
        }
    }
}

//MARK:- UISearchBarDelegate
extension UserListViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        arrFilterContents.removeAll()
        heightConstraint.constant = 0
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    
        arrFilterContents = arrPaginationContent.filter ({
            $0.name.contains(searchText) ||
                $0.email.contains(searchText) ||
                $0.role.contains(searchText)
        })
        
        isFiltered = (arrFilterContents.count == 0) ? false : true
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        heightConstraint.constant = 100
        isFiltered = false
        updatePaginationContent(page: 1)
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        heightConstraint.constant = 100
        isFiltered = false
        updatePaginationContent(page: 1)
        self.view.endEditing(true)
    }
}
