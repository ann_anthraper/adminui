//
//  UserTableViewCell.swift
//  AdminUI
//
//  Created by Ann Mary on 23/10/21.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var btnSelectCell: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var roleBGView: UIView!
    @IBOutlet weak var lblRole: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnEditProfile: UIButton!
    @IBOutlet weak var btnDeleteUser: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        customizeCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func customizeCell() {
        containerView.layer.cornerRadius = 10.0
        lblRole.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
    }
    
}
