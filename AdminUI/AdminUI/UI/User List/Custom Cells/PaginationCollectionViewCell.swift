//
//  PaginationCollectionViewCell.swift
//  AdminUI
//
//  Created by Ann Mary on 24/10/21.
//

import UIKit

class PaginationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
