//
//  Constants.swift
//  AdminUI
//
//  Created by Ann Mary on 23/10/21.
//

import Foundation
import RealmSwift

//URL Constants
let base_url = "https://geektrust.s3-ap-southeast-1.amazonaws.com/"
let endpoint = "adminui-problem/members.json"

//Pagination constant
let paginationDataLimit = 10
var maximumPageCount = 0

//Realm
let realm = try! Realm()

//Webservice completion handler
typealias WebServiceCompletionHandler = (_ status:Bool, _ message:String?, _ responseObject:AnyObject?) -> Void
